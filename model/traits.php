<?php
trait selectData {
    public function selectData($query) {
        $db = self::connect(); // Connect to database

        $stmt = $db->prepare($query);
        $stmt->execute();

        if($stmt->rowCount() > 0) {
            $temp_array = [];

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                array_push($temp_array, $row);
            }

            $db = null; // Disconnect database before the return of data;
            return $temp_array;
        } else {
            $db = null; // Disconnect database before the return of false;
            return false;
        }
    }
}