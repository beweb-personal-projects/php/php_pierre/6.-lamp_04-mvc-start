<?php
require('traits.php');

class ManagementDatabase extends Model {
    use selectData;

    public function getDatabaseTables() {
        $data_array = [];
        $data = $this->selectData("SHOW TABLES;");

        foreach ($data as $i => $table) {
            array_push($data_array, [
                $table["Tables_in_my_db"] => []
            ]);
        }

        return $data_array;
    }

    public function getDatabaseDetails() {
        $data_array = $this->getDatabaseTables();
        $data = $this->selectData(" SELECT * FROM INFORMATION_SCHEMA.COLUMNS
                                    WHERE TABLE_SCHEMA = N'my_db'
                                    ORDER BY TABLE_NAME, ORDINAL_POSITION ASC;"
        );

        foreach ($data as $i => $column) {
            foreach ($data_array as $j => $row) {
                if($column['TABLE_NAME'] === array_keys($row)[0]) {
                    array_push($data_array[$j][array_keys($row)[0]], [$column['COLUMN_NAME'], $column['DATA_TYPE']]);
                }
            }
        }

        return $data_array;
    }

    public function getDatabaseTable(string $table):array {
        $data = $this->selectData("SELECT * FROM `{$table}`;");

        return $data;
    }

    public function getDatabaseTableCount(string $table):int {
        $data = $this->selectData(" SELECT COUNT(*) as total_count
                                    FROM INFORMATION_SCHEMA.COLUMNS
                                    WHERE TABLE_SCHEMA = N'my_db' AND TABLE_NAME = '$table'
                                    ORDER BY TABLE_NAME, ORDINAL_POSITION ASC;");
        return (int) $data[0]['total_count'];
    }

    public function deleteTableRow(string $table, int $id) {
        $db = self::connect(); // Connect to database

        $data = [
            'id' => $id,
        ];

        $query = "DELETE FROM $table WHERE id = :id;";
        $stmt= $db->prepare($query);
        $stmt->execute($data);

        $db = null; // Disconnect database;
    }
}