<?php if($page['page_method'] === "setView"): ?>
    <!-- This is when we have all generical tables in the view -->
    <div class="container">
        <div class="row p-5">
            <?php foreach($page['data'] as $i => $table):?>
                <div class="card m-2 border border-dark" style="width: 18rem;">
                    <div class="card-body d-flex flex-column">
                        <h5 class="card-title text-center bg-primary text-light p-2"><?=array_keys($table)[0]?></h5>
                        <p class="card-text">
                            <?php 
                                foreach ($table as $j => $column) {
                                    for ($k=0; $k < count($column); $k++) { 
                                        echo "<p class='bg-light border p-1 m-0'>- " . $column[$k][0] . " (" . $column[$k][1] . ")<p>";
                                    }
                                }
                            ?>
                        </p>
                        <a href="<?=Controller::getHost()?>/management/seeDetails/<?=array_keys($table)[0]?>" class="btn btn-dark mt-auto">More Info</a>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
<?php else: ?>
    <!-- This is when we click in a specific Table -->
    <div class="container">
        <div class="row mt-3 text-center">
            <form action="<?=Controller::getHost()?>/management/setView">
                <input type="submit" class="btn btn-primary w-100" value="GO BACK"/>
            </form>
        </div>
        <table class="table table-striped table-dark mt-3">
            <thead>
                <tr>
                    <?php 
                        foreach($page['data_table'] as $i => $table):
                            if(Controller::getParams()[2] === array_keys($table)[0]):
                                foreach ($table as $j => $column):
                                    for ($k=0; $k < count($column); $k++):?>
                                        <th scope="col"><?=strtoupper($column[$k][0])?></th>
                                    <?php endfor;
                                endforeach;
                            endif;
                        endforeach;
                    ?>
                    <th scope="col" colspan="2">ACTIONS</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($page['data_content'] as $i => $value):?>
                    <tr>
                        <?php for ($i=0; $i < $page['data_columns_count']; $i++): ?>
                            <td><?=array_values($value)[$i]?></td>
                        <?php endfor; ?>
                
                        <form action="<?=Controller::getHost()?>/management/seeDetails/<?=$value['id']?>" method="POST">
                            <td>
                                <input type="submit" class="btn btn-warning" value="Edit"/>
                            </td>
                        </form>
                        <form action="<?=Controller::getHost()?>/management/deleteRow/<?=Controller::getParams()[2]?>/<?=$value['id']?>" method="POST">
                            <td>
                                <input type="submit" class="btn btn-danger" value="Delete"/>
                            </td>
                        </form>

                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
<?php endif; ?>