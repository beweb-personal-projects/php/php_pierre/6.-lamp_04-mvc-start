<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="<?=Controller::getHost()?>">OOP - MVC</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <?php foreach ($navbar as $i => $link):?>
            <li class="nav-item">
                <a class="nav-link" aria-current="page" href="<?=Controller::getHost()?>/<?=$link?>/setView"><?=ucfirst($link)?></a>
            </li>
        <?php endforeach?>
      </ul>
    </div>
  </div>
</nav>