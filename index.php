<?php

$get_params = explode("/", $_GET['p']);

// Declare as constants the current paths and remove of the index.php from the path.
define('WEBROOT' , str_replace('index.php' , '' , $_SERVER['SCRIPT_NAME']));
define('ROOT' , str_replace('index.php' , '' , $_SERVER['SCRIPT_FILENAME']));

// We load the Core/Generic Model right here so we can have access later on
require(ROOT.'core/model.php');
require(ROOT.'model/generics.php');

// We load the Core/Generic Controller right here so we can have access later on
require(ROOT.'core/controller.php');
require(ROOT.'controllers/generics.php');


// If we have the params we will declare the variables with the values, if not we will send the user directly to the homepage view.
if(isset($get_params[0]) && isset($get_params[1])) {
    $controller = $get_params[0] ; 
    $method = $get_params[1]; 

    if(isset($get_params[2])) {
        $methodParams = $get_params[2]; 
    }
} else {
    header("Location: " . ROOT.Controller::getHost() . "/homepage/setView");
}

// We require the specific controller and then we will verify if the method exists inside of the specific controller.
$current_page = ROOT."controllers/homepage.php";

foreach (Controller::getPages() as $i => $page) {
    if(Controller::removeExtentionName($page) === $controller) {
        $current_page = ROOT."controllers/". $page;
    }
}

require($current_page);

if (method_exists($controller, $method)) {
    $myctrl = new $controller() ; 

    if(isset($get_params[2])) {
        $myctrl->$method($methodParams);
    } else {
        $myctrl->$method(); 
    }
} else {
    header("Location: " . Controller::getHost() . "/homepage/setView");
}
