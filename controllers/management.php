<?php
class Management extends Controller {
    /**
     * This method exists to Send all the tables that exists in the database to the view
     *
     * @return void
     */
    public function setView():void {
        $data_obj = new ManagementDatabase();
        
        $data['page'] = [
            'page_name' => 'management',
            'page_method' => 'setView',
            'data' => $data_obj->getDatabaseDetails(),
        ];
        
        $this->set($data);
        $this->render('view');
    } 

    /**
     * This method exists to intermediate the data and send it to the specific selected Tables in the View
     *
     * @param string $table
     * @return void
     */
    public function seeDetails($table) {
        $data_obj = new ManagementDatabase();

        $data['page'] = [
            'page_name' => 'management',
            'page_method' => 'seeDetails',
            'data_table' => $data_obj->getDatabaseDetails(),
            'data_columns_count' => $data_obj->getDatabaseTableCount($table),
            'data_content' => $data_obj->getDatabaseTable($table),
        ];

        $this->set($data);
        $this->render('view');
    }

    public function deleteRow():void {
        $data_obj = new ManagementDatabase();
        $get_params = Controller::getParams();

        $data_obj->deleteTableRow($get_params[2], $get_params[3]);

        header("Location: " . Controller::getHost() . "/management/seeDetails/" . $get_params[2]);
    }
}