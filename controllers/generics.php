<?php

class Navbar extends Controller {
    public function setView() {
        $data['navbar'] = Controller::getPagesWithoutExtention();
        
        $this->set($data);
        $this->render('components/navbar');
    }
}