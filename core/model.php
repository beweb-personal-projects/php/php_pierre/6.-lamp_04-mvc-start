<?php 
class Model {
    public static function connect() {
        $user = 'root'; 
        $password = 'root';

        try {
            $db = new PDO("mysql:host=db;dbname=my_db", $user, $password);
            if($db) return $db;
        } catch (PDOException $e) {
            print "ERROR: " . $e->getMessage() . "<br/>";
            die();
        }

    }
}