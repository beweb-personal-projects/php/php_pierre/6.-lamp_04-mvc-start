<?php
class Controller {
    public $vars = array();
    
    public function set($data) {
        $this->vars = array_merge( $this->vars , $data );
    }

    public function render($filename){
        extract($this->vars);
        require(ROOT . 'views/' . $filename . '.php');
    }

    // ALL GETTERSSSS

    public static function getHost() {
        return "http://" . $_SERVER['HTTP_HOST'];
    }

    public static function getPages() {
        $path = scandir("./views/pages/");
        return array_splice($path, 2, count($path));
    }

    public static function getPagesWithoutExtention() {
        $temp_pages = [];

        foreach (Controller::getPages() as $i => $page) {
            array_push($temp_pages, Controller::removeExtentionName($page));
        }

        return $temp_pages;
    }

    // RANDOM METHODS

    public static function removeExtentionName($file) {
        $current_file = substr($file, 0, strrpos($file, "."));
        return $current_file;
    }

    public static function getParams(): array {
        return explode("/", $_GET['p']);
    }
}